const helloServices = require('./../services/helloServices');
const helloController = require('./../controller/hello_controller');

test('hello', () => {
    var expected = [ { appli: 'Back office magasin',
        hello: '{"back-office-magasin":"Hello World !"}',
        err: false },
        { appli: 'Caisse',
            hello: '{"caisse":"Hello World !"}',
            err: false },
        { appli: 'Décisionnel',
            hello: '{"decisionnel":"Hello World !"}',
            err: false },
        { appli: 'E-commerce',
            hello: '{"e-commerce":"Hello World !"}',
            err: false },
        { appli: 'Gestion commerciale',
            hello: '{"gestion-commerciale":"Hello World ! "}',
            err: false },
        { appli: 'Gestion entrepôts',
            hello: 'Error to fetch data',
            err: true },
        { appli: 'Gestion promotion',
            hello: '{"Gestion promotion":"Hello World!"}',
            err: false },
        { appli: 'Monétique paiement',
            hello: '{"monetique-paiement":"Hello World !"}',
            err: false },
        { appli: 'Référentiel produit',
            hello: '{"referentiel-produit":"Hello World !"}',
            err: false },
        { appli: 'Relation client',
            hello: '{"relation-client":"Hello World !"}',
            err: false } ];
    expect(helloServices.getHelloApps()).resolves.toBe(expected);
})