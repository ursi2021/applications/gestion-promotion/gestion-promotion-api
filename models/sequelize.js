const logger = require('../logs');
const { Sequelize } = require('sequelize');

// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);
const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER,process.env.URSI_DB_PASSWORD, {
    host: process.env.URSI_DB_HOST,
    port: Number(process.env.URSI_DB_PORT),
    logging: msg => logger.verbose(msg),
    dialectOptions: {
        timezone: 'Etc/GMT0'
    },
    dialect: 'mariadb'
});

try {
    sequelize.authenticate();
    logger.info('Connection has been established successfully.');
} catch (error) {
    logger.error('Unable to connect to the database:', error);
}

let db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
