const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");


/**
 * @swagger
 * definitions:
 *  Product list:
 *      type: object
 *      properties:
 *          offerId:
 *              type: foreignKey
 *          productCode:
 *              type: string
 */
class ProductList extends Sequelize.Model {}
ProductList.init(
    {
        offerId: {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false
        },
        productCode: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
    },
    {
        sequelize,
        ModelName: "productList",
        timestamps: false
        // Other model options go here
    }
);

ProductList.sync();
logger.info("The table for the ProductList model was just (re)created!");

module.exports = ProductList;
