const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");
var axios = require('axios');

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          firstName:
 *              type: string
 *          lastName:
 *              type: string
 */
class Product extends Sequelize.Model {}
Product.init(
    {
        'product-code': {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        'product-family': {
            type: Sequelize.DataTypes.STRING
            // allowNull defaults to true
        },
        'product-description': {
            type: Sequelize.DataTypes.STRING
            // allowNull defaults to true
        },
        'min-quantity': {
            type: Sequelize.DataTypes.INTEGER
            // allowNull defaults to true
        },
        'packaging': {
            type: Sequelize.DataTypes.INTEGER
            // allowNull defaults to true
        },
        'price': {
            type: Sequelize.DataTypes.FLOAT
            // allowNull defaults to true
        },
        'assortment-type': {
            type: Sequelize.DataTypes.STRING
            // allowNull defaults to true
        }
    },
    {
        sequelize,
        ModelName: "products",
        timestamps: false
        // Other model options go here
    }
);


Product.sync();
logger.info("The table for the Product model was just (re)created!");

Product.findAll().then(async function() {
    await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products').then(async function (response) {
        await Product.bulkCreate(response.data).catch(err => console.log(err.data));
    }).catch((err) => {
        console.error(err.data);
    });
});

module.exports = Product;
