const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");


/**
 * @swagger
 * definitions:
 *  Store list:
 *      type: object
 *      properties:
 *          offerId:
 *              type: foreignKey
 *          storeCode:
 *              type: string
 */
class StoreList extends Sequelize.Model {}
StoreList.init(
    {
        offerId: {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false
        },
        storeCode: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
    },
    {
        sequelize,
        ModelName: "storeList",
        timestamps: false
        // Other model options go here
    }
);

StoreList.sync();
logger.info("The table for the StoreList model was just (re)created!");

module.exports = StoreList;
