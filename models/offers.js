const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");


/**
 * @swagger
 * definitions:
 *  Offer:
 *      type: object
 *      properties:
 *          start_date:
 *              type: string (JJ-MM-AAAA hh:mm:ss)
 *          end_date:
 *              type: string (JJ-MM-AAAA hh:mm:ss)
 *          offer_percentage:
 *              type: float
 *          offer_amount:
 *              type: float
 *          client-segment-id:
 *              type: int
 *          promotion_type:
 *              type: string (web, magasin, client)
 *          minimum_amount:
 *              type: float
 *          description:
 *              typr: string
 *          clients_id:
 *              type: string
 */
class Offer extends Sequelize.Model {}
Offer.init(
    {
        // Model attributes are defined here
        'start-date': {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        'end-date': {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        'offer-percentage': {
            type: Sequelize.DataTypes.FLOAT
        },
        'offer-amount': {
            type: Sequelize.DataTypes.FLOAT
        },
        'client-segment-id': {
            type: Sequelize.DataTypes.STRING
        },
        'promotion-type': {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        'minimum-amount': {
            type: Sequelize.DataTypes.FLOAT,
            allowNull: false
        },
        'description': {
            type: Sequelize.DataTypes.STRING
        },
        'clients-id': {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        }
    },
    {
        sequelize,
        ModelName: "offers",
        timestamps: false
        // Other model options go here
    }
);

Offer.sync();
logger.info("The table for the Offer model was just (re)created!");

module.exports = Offer;
