const axios = require("axios");

async function getHelloApps() {
    let result = [];
    try {
        let applications = [
            {appli:'Back office magasin', err: false, url: process.env.URL_BACK_OFFICE_MAGASIN},
            {appli:'Caisse', err: false, url: process.env.URL_CAISSE},
            {appli:'Décisionnel', err: false, url: process.env.URL_DECISIONNEL},
            {appli:'E-commerce', err: false, url: process.env.URL_E_COMMERCE},
            {appli:'Gestion commerciale', err: false, url: process.env.URL_GESTION_COMMERCIALE},
            {appli:'Gestion entrepôts', err: false, url: process.env.URL_ENTREPROTS},
            {appli:'Gestion promotion', err: false, url: process.env.URL_GESTION_PROMOTION},
            {appli:'Monétique paiement', err: false, url: process.env.URL_MONETIQUE_PAIEMENT},
            {appli:'Référentiel produit', err: false, url: process.env.URL_REFERENTIEL_PRODUIT},
            {appli:'Relation client', err: false, url: process.env.URL_RELATION_CLIENT}
        ];
        for (let i = 0; i < applications.length; i++) {
            await axios.get(applications[i].url + '/hello' ).then(function (response) {
                result.push({appli: applications[i].appli, hello: JSON.stringify(response.data), err: applications[i].err});
            }).catch((error) => {
                applications[i].err = true;
                result.push({appli: applications[i].appli, hello: 'Error to fetch data', err: applications[i].err});
            });
        }
    } catch (e) {
        result.push(e);
    }
    return result;
}

module.exports.getHelloApps = getHelloApps;
