const axios = require("axios");
var Product = require('../models/products');
const logger = require("../logs");

async function getProducts() {
    await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products').then(async function (response) {
        await Product.truncate();
        response.data.forEach(it => delete(it.id));
        await Product.bulkCreate(response.data);
    }).catch((err) => {
        console.error(err.data);
    });
    let result;
    try {
        result = await Product.findAll();
    } catch (e) {
        result = {"myerror": "My error : return data type /products is undefined: maybe table is empty"};
    }
    if (result)
        return result;
    return {"Error": "return data type /products/ is undefined: maybe table is empty"};
}

async function getWebProducts() {
    logger.info(process.env.URL_REFERENTIEL_PRODUIT + '/products/web');
    await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products/web').then(async function (response) {
        await Product.destroy({
            where: {
                'assortment-type': ['Web']
            }
        });
        response.data.forEach(it => delete(it.id));
        await Product.bulkCreate(response.data);
    }).catch((err) => {
        console.log(err);
    });
    let result;
    try {
        result = await Product.findAll({
            where: {
                'assortment-type': ['Web']
            }
        });
    } catch (e) {
        result = {"Error": "return data type /products/web is undefined: maybe table is empty"};
    }
    if (result)
        return result;
    return {"Error": "return data type /products/web is undefined: maybe table is empty"};
}

module.exports.getProducts = getProducts;
module.exports.getWebProducts = getWebProducts;
