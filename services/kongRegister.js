var axios = require('axios');

var registered = false;

function register_kong() {
    try {
        axios
            .post('http://kong:8081/services/', {
                name: process.env.APP_NAME,
                url: 'http://' + process.env.APP_NAME
            })
            .then(res => {
                console.log(`statusCode: ${res.statusCode}`)
                console.log(res)
            })
            .catch(error => {
                console.error(error)
                return false;
            })

        axios
            .post('http://kong:8081/services/' + process.env.APP_NAME + '/routes', {
                paths: ["/" + process.env.APP_NAME],
                name: process.env.APP_NAME
            })
            .then(res => {
                console.log(`statusCode: ${res.statusCode}`)
                console.log(res)
            })
            .catch(error => {
                console.error(error)
                return false;
            })
        return true;
    } catch (e) {
        return false;
    }
}

while (registered === false) {
    registered = register_kong();
}
