const axios = require("axios");
var Offer = require('../models/offers');
var ProductList = require('../models/product_list');
var StoreList = require('../models/store_list');
var Product = require('../models/products');
const logger = require("../logs");

async function generateWebOffer() {
    try {
        var today = new Date();
        var anotherDate = new Date(today);
        anotherDate.setDate(today.getDate() + 1);
        var secondPart = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        var productCodes = await Product.findAll({
            where: {
                'assortment-type': ['Web', 'Both']
            },
            attributes: ['product-code'],
        }).catch(e => console.log({'error':  + e}));
        var firstProduct = Math.floor(Math.random() * productCodes.length);
        var secondProduct = Math.floor(Math.random() * productCodes.length);
        while (productCodes.length > 1 && firstProduct === secondProduct)
            secondProduct = Math.floor(Math.random() * productCodes.length);
        var randomOffer = {
            'start-date': today.getDate() + '-' + (today.getMonth()+1) + '-' + today.getFullYear() + ' ' + secondPart,
            'end-date': anotherDate.getDate() + '-' + (anotherDate.getMonth()+1) + '-' + anotherDate.getFullYear() + ' ' + secondPart,
            'offer-percentage': Math.floor((Math.random() * 10) + 5),
            'offer-amount': null,
            'client-segment-id': null,
            'promotion-type': 'web',
            'minimum-amount': Math.floor(Math.random() * 100),
            'description': 'Random offer',
            'clients-id': Math.floor(Math.random() * 1000).toString()
        };
        var newOffer = await Offer.create(randomOffer).catch();
        await ProductList.create({
            'offerId': newOffer.id,
            'productCode': productCodes[firstProduct]['product-code']
        }).catch(e => console.log(e));
        await ProductList.create({
            'offerId': newOffer.id,
            'productCode': productCodes[secondProduct]['product-code']
        }).catch(e => console.log(e));
        await StoreList.create({
            'offerId': newOffer.id,
            'storeCode' : 'store1'
        }).catch(e => console.log(e));
    } catch (e) {
        console.log(e);
    }
}

async function generateLeastSoldOffer() {
    try {
        var products;
        await axios.get(process.env.URL_DECISIONNEL + '/promotion').then(function (response) {
            products = response.data;
        }).catch(e => console.log({'error': e}));
        var product1 = "";
        var product2 = "";
        if (!products || products.length < 2) {
            var productCodes = await Product.findAll({
                attributes: ['product-code']
            }).catch(e => console.log({'error':  + e}));
            if (!productCodes || productCodes.length < 2)
                return;
            product1 = productCodes[0]['product-code'];
            product2 = productCodes[1]['product-code'];
        }
        else {
            product1 = products[0]['product-code'];
            product2 = products[1]['product-code'];
        }
        if (product1.length === 0 || product2.length === 0) {
            return;
        }
        var today = new Date();
        var anotherDate = new Date(today);
        anotherDate.setDate(today.getDate() + 1);
        var secondPart = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        var leastSold = {
            'start-date': today.getDate() + '-' + (today.getMonth()+1) + '-' + today.getFullYear() + ' ' + secondPart,
            'end-date': anotherDate.getDate() + '-' + (anotherDate.getMonth()+1) + '-' + anotherDate.getFullYear() + ' ' + secondPart,
            'offer-percentage': Math.floor((Math.random() * 15) + 5),
            'offer-amount': null,
            'client-segment-id': null,
            'promotion-type': 'leastsold',
            'minimum-amount': 0,
            'description': 'Offer for the two least sold products',
            'clients-id': Math.floor(Math.random() * 1000).toString()
        };
        var newOffer = await Offer.create(leastSold).catch();
        await ProductList.create({
            'offerId': newOffer.id,
            'productCode': product1
        }).catch(e => console.log(e));
        await ProductList.create({
            'offerId': newOffer.id,
            'productCode': product2
        }).catch(e => console.log(e));
        await StoreList.create({
            'offerId': newOffer.id,
            'storeCode' : 'store1'
        }).catch(e => console.log(e));
    } catch (e) {
        console.log(e);
    }
}

async function jsonOffers() {
    var offerjson = {};
    try {
        var offers = await Offer.findAll().catch(e => offerjson = {'error': 'an error occured while getting information in DB'});
        for (var i = 0; i < offers.length; ++i) {
            var productcodes = await ProductList.findAll({
                where: { 'offerId': offers[i]['id'] },
                attributes: ['productCode']
            }).catch(e => offerjson = {'error': 'an error occured while getting information in DB'});
            let product1 = "";
            let product2 = "";
            if (productcodes.length >= 1) {
                product1 = productcodes[0]['productCode'];
                if (productcodes.length > 1) {
                    product2 = productcodes[1]['productCode'];
                }
            }
            if (product1.length === 0 || product2.length === 0)
                continue;
            offerjson[i] = ({
                'id': offers[i]['id'],
                'start-date': offers[i]['start-date'],
                'end-date': offers[i]['end-date'],
                'offer-percentage': offers[i]['offer-percentage'],
                'offer-amount': offers[i]['offer-amount'],
                'stores-list': ['store1'],
                'promotion-type': offers[i]['promotion-type'],
                'products-list': [ product1, product2],
                'description': offers[i]['description'],
                'clients-id': offers[i]['clients-id']
            });
        }
        if (offers.length < 1)
            offerjson = {'Empty table': 'There is no offer currently. You can create an offer on /gestion-promotion/page1'};
    } catch (e) {
        offerjson = ({
            'error when getting offers': e
        });
    }
    return offerjson;
}

module.exports.jsonOffers = jsonOffers;
module.exports.generateWebOffer = generateWebOffer;
module.exports.generateLeastSoldOffer = generateLeastSoldOffer;
