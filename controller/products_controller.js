let express = require('express');
let app = express();

let controller = {}

module.exports = controller

const productServices =  require('../services/productsServices');

controller.getProducts = async function(req, res, next) {
    const result = await productServices.getProducts().catch(e => console.log({'error':  + e}));
    res.render('products', {title: 'Products', products: result});
}

controller.getWebProducts = async function(req, res, next) {
    const result = await productServices.getWebProducts().catch(e => console.log({'error':  + e}));
    res.render('products', {title: 'Web products', products: result});
}

