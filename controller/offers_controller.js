const offersServices =  require('../services/offersServices')
let controller = {}

module.exports = controller

controller.page1 = function(req, res, next) {
    res.render('page1', { title: 'Page 1' });
    res.end();
}

controller.generateWebOffer = async function(req, res, next) {
    await offersServices.generateWebOffer();
}

controller.generateLeastSoldOffer = async function(req, res, next) {
    await offersServices.generateLeastSoldOffer();
}

controller.jsonOffers = async function(req, res, next) {
    await controller.generateWebOffer(req, res, next);
    await controller.generateLeastSoldOffer(res, res, next);
    const result = await offersServices.jsonOffers().catch(e => console.log(e));
    res.status(200).json(result);
    res.end();
}
