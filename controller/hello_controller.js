const helloServices =  require('../services/helloServices')

let controller = {}

module.exports = controller

controller.hello = function(req, res, next) {
    res.send({'Gestion promotion': 'Hello World!'});
}

controller.helloAll = async function (req, res, next) {
    const result = await helloServices.getHelloApps().catch(e => console.log({'error':  + e}));
    res.render('hello', {hello: result});
}
